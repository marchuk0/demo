package com.example.demo.controller;

import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Objects;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerWithServiceIT {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ToDoRepository toDoRepository;

	@Test
	void whenGetAll_thenReturnValidResponse() throws Exception {
		String testText = "My to do text";
		when(toDoRepository.findAll()).thenReturn(
			Arrays.asList(
				new ToDoEntity(1l, testText)
			)
		);
		
		this.mockMvc
			.perform(get("/todos"))
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].text").value(testText))
			.andExpect(jsonPath("$[0].id").isNumber())
			.andExpect(jsonPath("$[0].completedAt").doesNotExist());
	}

	@Test
	void whenSaveValid_thenReturnValidResponse() throws Exception {
		//mock
		String testText = "Hll, Wrld";
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity toDoEntity = i.getArgument(0, ToDoEntity.class);
			Long id = toDoEntity.getId();
			String text = toDoEntity.getText();
			if(id == null) {
				return new ToDoEntity(0L, text);
			}
			return new ToDoEntity(Objects.requireNonNullElse(id, 1L), text);
		});

		//call and verify
		var body = new ToDoSaveRequest();
		body.text = testText;
		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(body)).characterEncoding("utf-8"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").exists())
				.andExpect(jsonPath("$.id").isNumber())
				.andExpect(jsonPath("$.text").value(testText))
				.andExpect(jsonPath("$.completedAt").doesNotExist());
	}

	@Test
	void whenSaveInvalid_thenReturnInvalidResponse() throws Exception {
		//mock
		String testText = "Hll, Wrld";
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> {
			ToDoEntity toDoEntity = i.getArgument(0, ToDoEntity.class);
			Long id = toDoEntity.getId();
			String text = toDoEntity.getText();
			return new ToDoEntity(Objects.requireNonNullElse(id, 1L), text);
		});

		//call
		var body = new ToDoSaveRequest();
		body.text = testText;
		body.id = 1L;
		this.mockMvc
				.perform(post("/todos")
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(body)).characterEncoding("utf-8"))
				.andExpect(status().isNotFound())
				.andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")));
	}


}
