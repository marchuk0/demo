package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@ActiveProfiles(profiles = "test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
class ToDoControllerWithServiceAndRepositoryIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ToDoRepository toDoRepository;

    @BeforeEach
    void initRepository() {
        toDoRepository.save(new ToDoEntity(1L, "Wash the dishes"));
        toDoRepository.save(new ToDoEntity(2L, "Learn to test Java app").completeNow());
    }

    @Test
    void whenComplete_thenReturnValidResponse() throws Exception {
        //call and verify
        this.mockMvc
                .perform(put("/todos/1/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("Wash the dishes"))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenCompleteNonexistent_thenReturnInvalidResponse() throws Exception {
        //call
        this.mockMvc
                .perform(put("/todos/101/complete"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")));
    }

    @Test
    void whenGetAll_thenReturnAll() throws Exception {
        //call
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].text").value("Wash the dishes"))
                .andExpect(jsonPath("$[0].completedAt").doesNotExist());
    }

    @Test
    void whenGetValid_thenReturnValidResponse() throws Exception {
        //call
        this.mockMvc
                .perform(get("/todos/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.text").value("Wash the dishes"))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetNonexistent_thenReturnInvalidResponse() throws Exception {
        //call
        this.mockMvc
                .perform(get("/todos/134"))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")));
    }

    @Test
    void whenDelete_thenReturnValidResponseAndDeleteFromDB() throws Exception {
        //call
        this.mockMvc
                .perform(delete("/todos/1"))
                .andExpect(status().isOk());

        assertTrue(toDoRepository.findById(1L).isEmpty());
    }

    @Test
    void whenDeleteAll_thenReturnValidResponseAndDeleteFromDb() throws Exception {
        this.mockMvc
                .perform(delete("/todos"))
                .andExpect(status().isOk());

        assertTrue(toDoRepository.findAll().isEmpty());
    }
}
